CREATE TABLE IF NOT EXISTS persistent_logins (
  username VARCHAR(64) NOT NULL,
  series VARCHAR(64) NOT NULL,
  token VARCHAR(64) NOT NULL,
  last_used TIMESTAMP NOT NULL,
  PRIMARY KEY (series)
);

-- INSERT INTO users(id, email, name, loginpage, password, role)
--   SELECT 1, 'letitsnowflyorgo@gmail.com', 'Администратор', 'admin', '$2a$10$3NH7OGRDOyg767yZoMcAZO4tAbueH7kSIt2WmgPX0ZEUbEFahVftG', 'ADMIN'
--   WHERE
--     NOT EXISTS(
--         SELECT id
--         FROM users
--         WHERE id = 1
--     );