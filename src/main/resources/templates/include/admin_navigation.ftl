<nav class="navbar navbar-invers">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/admin/">Home</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Adding <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="/admin/add/institution">Institution</a></li>
                        <li><a href="/admin/add/subject">Subject</a></li>
                        <li><a href="/admin/add/teacher">Teacher</a></li>
                        <li><a href="/admin/add/student">Student</a></li>
                        <li><a href="/admin/add/group">Group</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/logout"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
            </ul>
        </div>
    </div>
</nav>