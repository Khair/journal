<#ftl encoding='UTF-8'>
<#import "/spring.ftl" as spring />
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admin</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href="/">
    <title>Login</title>

    <script type="application/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="application/javascript" src="../js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/local/nav.css">
</head>
<body>
<#include "include/admin_navigation.ftl">
<#if error??>
<div style="margin: 0; text-align: center" class="alert alert-danger" role="alert">${error}</div>
<#elseif message??>
<div style="margin: 0; text-align: center" class="alert alert-success" role="alert">${message}</div>
</#if>
<div class="container">
    <div class="row">
        <div class="col-lg-offset-1 col-lg-10">
            <div class="jumbotron" style="background-color: white">
                <h1>
                    Add student for ${institution.name}
                </h1>
                <h2>
                    ${group.name} group
                </h2>
            </div>
            <form id="login-form" action="/admin/add/student" enctype="multipart/form-data" name="addStudent" method="post" role="form" style="display: block;">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <#--<@spring.bind path="studentDto.firstName"/>-->
                            <input class="form-control" align="center" type="text" placeholder="Student first name" name="firstName" value=""/>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                        <#--<@spring.bind path="studentDto.secondName"/>-->
                            <input class="form-control" align="center" type="text" placeholder="Student second name" name="secondName" value=""/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                        <#--<@spring.bind path="studentDto.fatherName"/>-->
                            <input class="form-control" align="center" type="text" placeholder="Student father name" name="fatherName" value=""/>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                        <#--<@spring.bind path="studentDto.birthday"/>-->
                            <input class="form-control" align="center" type="text" placeholder="Student birthday" name="birthday" value=""/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                        <#--<@spring.bind path="studentDto.citizenship"/>-->
                            <input class="form-control" align="center" type="text" placeholder="Student citizenship" name="citizenship" value=""/>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                        <#--<@spring.bind path="studentDto.birthCity"/>-->
                            <input class="form-control" align="center" type="text" placeholder="Student birthCity" name="birthCity" value=""/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-lg-offset-3">
                        <div class="form-group">
                            <input type="hidden" name="institutionId" value="${institution.id}">
                            <input type="hidden" name="groupId" value="${group.id}">
                            <input type="submit" id="login-submit" tabindex="4" class="form-control btn btn-primary" value="Add student">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-10 col-lg-offset-1">
            <h2 style="margin-top: 3%; margin-bottom: 3%">Added students</h2>
            <div class="panel-group">
            <#list students as student>
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h4 class="panel-title">${student.firstName} ${student.secondName}</h4>
                    </div>
                </div>
            </#list>
            </div>
        </div>
    </div>
</div>
</body>
</html>