<#ftl encoding='UTF-8'>
<#import "/spring.ftl" as spring />
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admin</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href="/">
    <title>Login</title>

    <script type="application/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="application/javascript" src="../js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/local/nav.css">
</head>
<body>
<#include "include/teacher_navigation.ftl">
<div class="container">
    <div class="col-lg-10 col-lg-offset-1">
    <#if students??>
    <div class="jumbotron" style="background-color: white">
        <h1>
            Choose student
        </h1>
    </div>
        <div class="panel-group">
        <#list students as student>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="/teacher/subject${subjectId}/group${group.id}/student${student.id}">
                        <h4 class="panel-title">${student.firstName} ${student.secondName}</h4>
                    </a>
                </div>
            </div>
        </#list>
        </div>
    <#else>
        <h1>
            You have not students
        </h1>
    </#if>
    </div>
</div>
</body>
</html>