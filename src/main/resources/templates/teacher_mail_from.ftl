<#ftl encoding='UTF-8'>
<#import "/spring.ftl" as spring />
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admin</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href="/">
    <title>Login</title>

    <script type="application/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="application/javascript" src="../js/bootstrap.min.js"></script>
    <script type="application/javascript" src="../js/local/search.js"></script>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/local/nav.css">
</head>
<body>
<#include "include/teacher_navigation.ftl">
<#if error??>
<div style="margin: 0; text-align: center" class="alert alert-danger" role="alert">${error}</div>
<#elseif message??>
<div style="margin: 0; text-align: center" class="alert alert-success" role="alert">${message}</div>
</#if>
<div class="container">
<#if letters??>
    <#if 0 < letters?size>
        <div class="row">
            <div class="col-lg-offset-1 col-lg-10">
                <div class="jumbotron" style="background-color: white">
                    <h1>
                        Your mail
                    </h1>
                </div>
                <div class="col-lg-12" style="margin-bottom: 3%">
                    <label for="searchInput">Search by key words</label>
                    <input class="form-control" id="searchInput" type="text" placeholder="Search..">
                </div>
                <div class="col-lg-12">
                <div class="panel-group" id="searchField">
                    <#list letters as letter>
                        <div class="panel panel-default" >
                            <div class="panel-heading">
                                <a href="/student/mailto${letter.student.id}">
                                    <h4 class="panel-title" id="namefrom">From: ${letter.student.firstName} ${letter.student.secondName}</h4>
                                </a>
                            </div>
                            <div class="panel-body">
                                <div class="row" style="padding-left: inherit; padding-right: inherit">
                                    <h4><b>Theme: ${letter.theme}</b></h4>
                                </div>
                                <div class="row" style="padding-left: inherit; padding-right: inherit">
                                    <h4>${letter.letter}</h4>
                                </div>
                            </div>
                        </div>
                    </#list>
                </div>
                </div>
            </div>
        </div>
    <#else >
        <div class="row">
            <div class="col-lg-offset-1 col-lg-10">
                <h1>
                    You do not have letters
                </h1>
            </div>
        </div>
    </#if>
<#else>
    <div class="row">
        <div class="col-lg-offset-1 col-lg-10">
            <h1>
                You do not have letters
            </h1>
        </div>
    </div>
</#if>
</div>
</body>
</html>