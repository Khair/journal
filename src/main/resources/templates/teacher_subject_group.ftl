<#ftl encoding='UTF-8'>
<#import "/spring.ftl" as spring />
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admin</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href="/">
    <title>Login</title>

    <script type="application/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="application/javascript" src="../js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/local/nav.css">
</head>
<body>
<#include "include/teacher_navigation.ftl">
<div class="container">
    <div class="col-lg-10 col-lg-offset-1">
        <#if subject?? && groups??>
            <div class="jumbotron" style="background-color: white">
                <h1>
                    Choose group
                </h1>
            </div>
            <div class="panel-group">
                <#list groups as group>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="/teacher/subject${subject.id}/group${group.id}">
                                <h4 class="panel-title">${group.name}</h4>
                            </a>
                        </div>
                    </div>
                </#list>
            </div>
        <#else>
            <h1>
                Create institutions please
            </h1>
        </#if>
    </div>
</div>
</body>
</html>