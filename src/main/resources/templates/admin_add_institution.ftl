<#ftl encoding='UTF-8'>
<#import "/spring.ftl" as spring />
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admin</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href="/">
    <title>Login</title>

    <script type="application/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="application/javascript" src="../js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/local/nav.css">
</head>
<body>
<#include "include/admin_navigation.ftl">
<#if error??>
<div style="margin: 0; text-align: center" class="alert alert-danger" role="alert">${error}</div>
<#elseif message??>
<div style="margin: 0; text-align: center" class="alert alert-success" role="alert">${message}</div>
</#if>
<div class="container">
    <div class="row">
        <div class="col-lg-10 col-lg-offset-1">
            <div class="jumbotron" style="background-color: white">
                <h1>
                    Add institution
                </h1>
            </div>
            <form id="login-form" action="/admin/add/institution" method="post" role="form" style="display: block;">
                <div class="row">
                    <div class="col-lg-9">
                        <div class="form-group">
                            <input type="text" name="name" id="name" tabindex="1" class="form-control" placeholder="Institution name" value="">
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <input type="submit" id="login-submit" tabindex="4" class="form-control btn btn-primary" value="Add institution">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <#if institutions??>
        <div class="row">
            <div class="col-lg-offset-1 col-lg-10">
                <h2 style="margin-top: 3%; margin-bottom: 3%">Added institutions</h2>
                <div class="panel-group">
                    <#list institutions as subject>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    ${subject.name}
                                </h4>
                            </div>
                        </div>
                    </#list>
                </div>
            </div>
        </div>
    </#if>
</div>
</body>
</html>