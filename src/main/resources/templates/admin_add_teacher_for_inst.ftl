<#ftl encoding='UTF-8'>
<#import "/spring.ftl" as spring />
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admin</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href="/">
    <title>Login</title>

    <script type="application/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="application/javascript" src="../js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/local/nav.css">
</head>
<body>
<#include "include/admin_navigation.ftl">
<#if error??>
<div style="margin: 0; text-align: center" class="alert alert-danger" role="alert">${error}</div>
<#elseif message??>
<div style="margin: 0; text-align: center" class="alert alert-success" role="alert">${message}</div>
</#if>
<div class="container">
    <div class="row">
        <div class="col-lg-offset-1 col-lg-10">
            <div class="jumbotron" style="background-color: white">
                <h1>
                    Add teacher for ${institution.name}
                </h1>
            </div>
            <form id="login-form" action="/admin/add/teacher" enctype="multipart/form-data" name="addStudent" method="post" role="form" style="display: block;">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                        <@spring.bind path="groupAndSubjectWrapper.firstname"/>
                            <input class="form-control" align="center" type="text" placeholder="Student first name" name="${spring.status.expression}" value=""/>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                        <@spring.bind path="groupAndSubjectWrapper.secondname"/>
                            <input class="form-control" align="center" type="text" placeholder="Student second name" name="${spring.status.expression}" value=""/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                        <@spring.bind path="groupAndSubjectWrapper.fathername"/>
                            <input class="form-control" align="center" type="text" placeholder="Student father name" name="${spring.status.expression}" value=""/>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                        <@spring.bind path="groupAndSubjectWrapper.birthday"/>
                            <input class="form-control" align="center" type="text" placeholder="Student birthday" name="${spring.status.expression}" value=""/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                        <@spring.bind path="groupAndSubjectWrapper.citizenship"/>
                            <input class="form-control" align="center" type="text" placeholder="Student citizenship" name="${spring.status.expression}" value=""/>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                        <@spring.bind path="groupAndSubjectWrapper.birthplace"/>
                            <input class="form-control" align="center" type="text" placeholder="Student birthCity" name="${spring.status.expression}" value=""/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <#list groupAndSubjectWrapper.groupCheckDtos as subject>
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <h4>Group ${subject.name}</h4>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <@spring.bind path="groupAndSubjectWrapper.groupCheckDtos[${subject_index}].checked"/>
                                        <input class="form-control" type="checkbox" name="${spring.status.expression}" value="${subject.checked?c}" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <@spring.bind path="groupAndSubjectWrapper.groupCheckDtos[${subject_index}].name"/>
                        <input class="custom-control-input" style="display: none" type="hidden" name="${spring.status.expression}" value="${subject.name}" />
                        <@spring.bind path="groupAndSubjectWrapper.groupCheckDtos[${subject_index}].groupid"/>
                        <input class="custom-control-input" style="display: none" type="hidden" name="${spring.status.expression}" value="${subject.groupid}" />
                    </#list>
                </div>
                <hr>
                <div class="row">
                    <#list groupAndSubjectWrapper.subjectCheckDtos as subject>
                        <div class="col-lg-6">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <h4>Subject ${subject.name}</h4>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <@spring.bind path="groupAndSubjectWrapper.subjectCheckDtos[${subject_index}].checked"/>
                                        <input class="form-control" type="checkbox" name="${spring.status.expression}" value="${subject.checked?c}" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <@spring.bind path="groupAndSubjectWrapper.subjectCheckDtos[${subject_index}].name"/>
                        <input class="custom-control-input" style="display: none" type="hidden" name="${spring.status.expression}" value="${subject.name}" />
                        <@spring.bind path="groupAndSubjectWrapper.subjectCheckDtos[${subject_index}].subjectid"/>
                        <input class="custom-control-input" style="display: none" type="hidden" name="${spring.status.expression}" value="${subject.subjectid}" />
                    </#list>
                </div>
                <div class="form-group">
                    <input class="custom-control-input" type="hidden" name="institutionName" value="${institution.name}">
                </div>
                <div class="form-group">
                    <input class="custom-control-input" type="hidden" name="institutionId" value="${institution.id}">
                </div>
                <div class="row">
                    <div class="col-lg-6 col-lg-offset-3">
                        <div class="form-group">
                            <input type="submit" id="login-submit" tabindex="4" class="form-control btn btn-primary" value="Add teacher">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-10 col-lg-offset-1">
            <h2 style="margin-top: 3%; margin-bottom: 3%">Added teachers</h2>
            <div class="panel-group">
                <#list teachers as subject>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h4 class="panel-title">${subject.firstName} ${subject.secondName}</h4>
                        </div>
                    </div>
                </#list>
            </div>
        </div>
    </div>
</div>
</body>
</html>