<#ftl encoding='UTF-8'>
<#import "/spring.ftl" as spring />
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admin</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href="/">
    <title>Login</title>

    <script type="application/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="application/javascript" src="../js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/local/nav.css">
</head>
<body>
<#include "include/student_navigation.ftl">
<#if error??>
<div style="margin: 0; text-align: center" class="alert alert-danger" role="alert">${error}</div>
<#elseif message??>
<div style="margin: 0; text-align: center" class="alert alert-success" role="alert">${message}</div>
</#if>
<div class="container">
<#if teachers??>
    <div class="row">
        <div class="col-lg-offset-1 col-lg-10">
            <div class="jumbotron" style="background-color: white">
                <h1>
                    Choose teacher
                </h1>
            </div>
            <div class="panel-group">
                <#list teachers as teacher>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a href="/student/mailto${teacher.id}">
                                <h4 class="panel-title">${teacher.firstName} ${teacher.secondName}</h4>
                            </a>
                        </div>
                    </div>
                </#list>
            </div>
        </div>
    </div>
<#else>
    <div class="row">
        <div class="col-lg-offset-1 col-lg-10">
            <h1>
                You do not have teachers
            </h1>
        </div>
    </div>
</#if>
</div>
</body>
</html>