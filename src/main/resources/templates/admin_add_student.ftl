<#ftl encoding='UTF-8'>
<#import "/spring.ftl" as spring />
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admin</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href="/">
    <title>Login</title>

    <script type="application/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="application/javascript" src="../js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/local/nav.css">
</head>
<body>
<#include "include/admin_navigation.ftl">
<#if error??>
<div style="margin: 0; text-align: center" class="alert alert-danger" role="alert">${error}</div>
</#if>
<div class="container">
    <div class="col-lg-10 col-lg-offset-1">
        <#if institutions??>
        <div class="jumbotron" style="background-color: white">
            <h1>
                Choose Institute and group
            </h1>
        </div>
        <div class="panel-group" id="accordion">
        <#list institutions as inst>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse${inst_index}">
                        ${inst.name}</a>
                    </h4>
                </div>
                <div id="collapse${inst_index}" class="panel-collapse collapse">
                    <ul class="list-group">
                        <#list inst.groups as group>
                            <li class="list-group-item">
                                <a href="/admin/add/student${inst.id}-${group.id}">Group ${group.name}</a>
                            </li>
                        </#list>
                    </ul>
                </div>
            </div>
        </#list>
        <#else>
            <h1>
                Create institutions please
            </h1>
        </#if>
        </div>
    </div>
</div>
</body>
</html>