<#ftl encoding='UTF-8'>
<#import "/spring.ftl" as spring />
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admin</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href="/">
    <title>Login</title>

    <script type="application/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="application/javascript" src="../js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/local/nav.css">
    <link rel="stylesheet" type="text/css" href="../css/local/slider.css">

</head>
<body>
<#include "include/teacher_navigation.ftl">
<#if error??>
<div style="margin: 0; text-align: center" class="alert alert-danger" role="alert">${error}</div>
<#elseif message??>
<div style="margin: 0; text-align: center" class="alert alert-success" role="alert">${message}</div>
</#if>
<div class="container">
    <div class="row">
        <div class="col-lg-10 col-lg-offset-1">
            <div class="jumbotron" style="background-color: white">
                <h1>
                    Add points and commentary
                </h1>
                <h2>
                ${subjectName.name}
                </h2>
            </div>
            <form id="login-form" action="/teacher/points" method="post" role="form" style="display: block;">
                <div class="row">
                    <div class="col-lg-12">
                        <h2>${student.firstName} ${student.secondName}</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <input type="text" name="comment" id="name" tabindex="1" class="form-control" placeholder="Comment" value="">
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="slidecontainer">
                            <input type="range" name="points" min="1" max="50" value="1" class="slider" id="myRange">
                            <p>Value: <span id="demo"></span></p>
                        </div>
                    </div>
                    <input type="hidden" name="subjectId" value="${subjectId}">
                    <input type="hidden" name="studentId" value="${student.id}">
                    <input type="hidden" name="groupId" value="${groupId}">
                    <div class="col-lg-6 col-lg-offset-3">
                        <div class="form-group">
                            <input type="submit" id="login-submit" tabindex="4" class="form-control btn btn-primary" value="Add points">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <#if points??>
        <#if 0 < points?size >
        <div class="row">
            <div class="col-lg-offset-1 col-lg-10">
                <h2 style="margin-top: 3%; margin-bottom: 3%">Added points</h2>
                <div class="panel-group">
                    <#list points as subject>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <h4 class="panel-title">
                                            Points ${subject.points}
                                        </h4>
                                    </div>
                                    <div class="col-lg-8">
                                        <h4 class="panel-title">
                                            ${subject.comment}
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </#list>
                </div>
            </div>
        </div>
        </#if>
    </#if>
</div>
</body>
<script type="application/javascript" src="../js/local/slider.js"></script>
</html>