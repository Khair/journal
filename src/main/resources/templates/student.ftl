<#ftl encoding='UTF-8'>
<#import "/spring.ftl" as spring />
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admin</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href="/">
    <title>Login</title>

    <script type="application/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="application/javascript" src="../js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/local/nav.css">
    <style>
        .points-container{overflow:hidden;width:100%; display: flex}
        .box{white-space:nowrap; margin-bottom: auto; margin-top: auto}
        .box div{display:inline-block;}
        .progress{height: 40px;width: 100%}
    </style>
</head>
<body>
<#include "include/student_navigation.ftl">
<div class="container">
    <div class="col-lg-10 col-lg-offset-1">
        <div class="jumbotron" style="background-color: white;">
            <h1>Points</h1>
        </div>
        <#if mapk??>
            <#list mapk as entity, p>
                <div class="row" style="margin-top: 16px; display: flex;">
                    <div class="col-lg-4" style="margin-top: auto; margin-bottom: auto">
                        <h4>${entity.name}</h4>
                    </div>
                    <div class="col-lg-8" style="margin-bottom: auto; margin-top: auto">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="points-container">
                                    <div class="box progress">
                                        <#if 0 < p?size >
                                            <#list p as val>
                                                <div class="progress-bar" data-toggle="popover" title="Comment" data-trigger="hover" data-placement="top" data-content="${val.comment}" style="background-color: #337ab7; width: ${val.points*2}%;
                                                    <#if 0 < val_index >
                                                            border-left: solid; border-left-width: 1px;
                                                    </#if>">
                                                    <h4 align="center">
                                                    ${val.points}
                                                    </h4>
                                                </div>
                                            </#list>
                                        <#else>
                                            <div style="width: inherit"><h4 align="center">0</h4></div>
                                        </#if>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </#list>
        </#if>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('[data-toggle="popover"]').popover();
    });
</script>
</body>
</html>