<#ftl encoding='UTF-8'>
<#import "/spring.ftl" as spring />
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admin</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href="/">
    <title>Login</title>

    <script type="application/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="application/javascript" src="../js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/local/nav.css">
</head>
<body>
<#include "include/admin_navigation.ftl">
    <div class="container">
        <div class="col-lg-10 col-lg-offset-1">
            <div class="jumbotron" style="background-color: white">
                <h1>Возможности администратора</h1>
            </div>
            <div class="panel panel-default">
                <div class="panel-body"><h3>Вы можете добавить институт</h3></div>
            </div>
            <div class="panel panel-default">
                <div class="panel-body"><h3>Вы можете добавить группу</h3></div>
            </div>
            <div class="panel panel-default">
                <div class="panel-body"><h3>Вы можете добавить преподавателя</h3></div>
            </div>
            <div class="panel panel-default">
                <div class="panel-body"><h3>Вы можете добавить студента</h3></div>
            </div>
            <div class="panel panel-default">
                <div class="panel-body"><h3>Вы можете добавить предмет</h3></div>
            </div>
        </div>
    </div>
</body>
</html>