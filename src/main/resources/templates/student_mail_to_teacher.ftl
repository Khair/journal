<#ftl encoding='UTF-8'>
<#import "/spring.ftl" as spring />
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admin</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href="/">
    <title>Login</title>

    <script type="application/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="application/javascript" src="../js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../css/local/nav.css">
</head>
<body>
<#include "include/student_navigation.ftl">
<#if error??>
<div style="margin: 0; text-align: center" class="alert alert-danger" role="alert">${error}</div>
<#elseif message??>
<div style="margin: 0; text-align: center" class="alert alert-success" role="alert">${message}</div>
</#if>
<div class="container">
    <div class="row">
        <div class="col-lg-offset-1 col-lg-10">
            <div class="jumbotron" style="background-color: white">
                <h1>
                    Write letter for
                </h1>
                <h2>
                    ${teacher.firstName} ${teacher.secondName}
                </h2>
            </div>
            <form id="login-form" action="/student/mailto" method="post" role="form" style="display: block;">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <input class="form-control" align="center" type="text" placeholder="Theme" name="theme" value="" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="letter">Letter:</label>
                            <textarea class="form-control" rows="5" id="letter" name="letter"></textarea>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-lg-offset-3">
                    <div class="form-group">
                        <input type="submit" id="login-submit" tabindex="4" class="form-control btn btn-primary" value="Add subject">
                    </div>
                </div>
                <div class="form-group" style="display: none">
                    <input class="custom-control-input" type="hidden"  name="teacherId" value="${teacher.id}">
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>