package ru.itis.security.roles;

public enum Role {
    ADMIN,
    USER,
    TEACHER;
}
