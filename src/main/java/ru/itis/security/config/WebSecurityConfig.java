package ru.itis.security.config;
/*
* @author Rustem Khairutdinov 
* @group 11-602
*/

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import ru.itis.repositories.UserRepository;
import ru.itis.security.authsuccesshandler.RedirectUrlAuthenticationSuccessHandler;
import ru.itis.security.details.UserDetailsServiceImpl;
import ru.itis.security.providers.AuthProvider;

import javax.sql.DataSource;

@EnableWebSecurity
@ComponentScan("ru.itis")
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final DataSource dataSource;
    public static final int TOKEN_VALIDITY_SECONDS = 24 * 60 * 60; // 1 day

    @Autowired
    private AuthProvider authProvider;

    @Autowired
    private UserDetailsServiceImpl userDetailsServiceImpl;

    public WebSecurityConfig(@Qualifier("dataSource") DataSource dataSource){
        this.dataSource = dataSource;
    }

    @Bean
    public PersistentTokenRepository persistentTokenRepository() {
        JdbcTokenRepositoryImpl jdbcTokenRepository = new JdbcTokenRepositoryImpl();
        jdbcTokenRepository.setDataSource(dataSource);
        return jdbcTokenRepository;
    }

    @Bean
    public AuthenticationSuccessHandler authenticationSuccessHandler(){
        return new RedirectUrlAuthenticationSuccessHandler();
    }

    @Bean
    public static PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                    .antMatchers("/").permitAll()
                    .antMatchers("/reg").permitAll()
                    .antMatchers("/admin**").hasAuthority("ADMIN")
                    .antMatchers("/student**").hasAuthority("STUDENT")
                    .antMatchers("/teacher**").hasAuthority("TEACHER")
                    .antMatchers("/css/**").permitAll()
                    .antMatchers("/js/**").permitAll()
                    .antMatchers("/register").permitAll()
                    .antMatchers("/img/**").permitAll()
                    .antMatchers("/fonts/**").permitAll()
                    .antMatchers("/files/**").permitAll()
                    .anyRequest().authenticated()
                .and()
                    .formLogin().loginPage("/")
                    .usernameParameter("login")
                    //.loginProcessingUrl("/j_spring_security_check")
                    .successHandler(authenticationSuccessHandler())
                    //.defaultSuccessUrl("/error_page")
                    //.defaultSuccessUrl("/authority-check")
                    .failureForwardUrl("/login?error")
                    .permitAll()
                .and()
                    .logout()
                    .logoutUrl("/logout")
                    .deleteCookies("remember-me")
                    .logoutSuccessUrl("/")
                    .permitAll()
                .and()
                    .rememberMe()
                    .key("uniqueAndSecret")
                    .userDetailsService(userDetailsServiceImpl)
                    .rememberMeParameter("remember-me")
                    .tokenRepository(persistentTokenRepository())
                    .tokenValiditySeconds(TOKEN_VALIDITY_SECONDS)
                .and()
                    .csrf()
                    .disable()
                    .authenticationProvider(authProvider);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("user")
                .password(passwordEncoder().encode("user"))
                .roles("ADMIN");
    }
}

