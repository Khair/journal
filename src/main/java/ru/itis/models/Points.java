package ru.itis.models;
/*
* @author Rustem Khairutdinov 
* @group 11-602
*/

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="\"points\"")
public class Points implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Long points;
    private String comment;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "student_id")
    private Student student;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "subject_id")
    private Subject subject;

    public Teacher getTeacher() {
        return teacher;
    }

    public Points setTeacher(Teacher teacher) {
        this.teacher = teacher;
        return this;
    }

    public Points(Long points, String comment, Student student, Subject subject, Teacher teacher) {

        this.points = points;
        this.comment = comment;
        this.student = student;
        this.subject = subject;
        this.teacher = teacher;
    }

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "teacher_id")
    private Teacher teacher;

    public Points() {

    }

    public Points(Long id, Long points, String comment, Student student, Subject subject) {
        this.id = id;
        this.points = points;
        this.comment = comment;
        this.student = student;
        this.subject = subject;
    }

    public Long getId() {
        return id;
    }

    public Points setId(Long id) {
        this.id = id;
        return this;
    }

    public Long getPoints() {
        return points;
    }

    public Points setPoints(Long points) {
        this.points = points;
        return this;
    }

    public String getComment() {
        return comment;
    }

    public Points setComment(String comment) {
        this.comment = comment;
        return this;
    }

    public Student getStudent() {
        return student;
    }

    public Points setStudent(Student student) {
        this.student = student;
        return this;
    }

    public Subject getSubject() {
        return subject;
    }

    public Points setSubject(Subject subject) {
        this.subject = subject;
        return this;
    }
}
