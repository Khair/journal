package ru.itis.models;
/*
* @author Rustem Khairutdinov 
* @group 11-602
*/

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "\"subject\"")
public class Subject implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name; // имя

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "course_id")
    private List<Course> courses = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "institution_id")
    private Institution institution;

    @OneToMany(
            fetch = FetchType.LAZY,
            mappedBy = "subject",
            cascade = CascadeType.ALL
    )
    private List<Points> points = new ArrayList<>();

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "teacher_id")
    private List<Teacher> teachers = new ArrayList<>();

    public Subject() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Subject setName(String name) {
        this.name = name;
        return this;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public Subject setCourses(List<Course> courses) {
        this.courses = courses;
        return this;
    }

    public Institution getInstitution() {
        return institution;
    }

    public Subject setInstitution(Institution institution) {
        this.institution = institution;
        return this;
    }

    public List<Points> getPoints() {
        return points;
    }

    public Subject setPoints(List<Points> points) {
        this.points = points;
        return this;
    }

    public List<Teacher> getTeachers() {
        return teachers;
    }

    public Subject setTeachers(List<Teacher> teachers) {
        this.teachers = teachers;
        return this;
    }
}
