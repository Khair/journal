package ru.itis.models;
/*
* @author Rustem Khairutdinov 
* @group 11-602
*/

import javax.persistence.*;

@Entity
@Table(name = "\"letter\"")
public class Letter {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String theme;
    private String letter;
    private Boolean fromStudent;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "student_id")
    private Student student;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "subject_id")
    private Teacher teacher;

    public Letter(String theme, String letter, Student student, Teacher teacher, Boolean fromStudent) {
        this.theme = theme;
        this.letter = letter;
        this.student = student;
        this.teacher = teacher;
        this.fromStudent = fromStudent;
    }

    public Boolean getFromStudent() {
        return fromStudent;
    }

    public Letter setFromStudent(Boolean fromStudent) {
        this.fromStudent = fromStudent;
        return this;
    }

    public Letter() {
    }

    public Long getId() {
        return id;
    }

    public Letter setId(Long id) {
        this.id = id;
        return this;
    }

    public String getTheme() {
        return theme;
    }

    public Letter setTheme(String theme) {
        this.theme = theme;
        return this;
    }

    public String getLetter() {
        return letter;
    }

    public Letter setLetter(String letter) {
        this.letter = letter;
        return this;
    }

    public Student getStudent() {
        return student;
    }

    public Letter setStudent(Student student) {
        this.student = student;
        return this;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public Letter setTeacher(Teacher teacher) {
        this.teacher = teacher;
        return this;
    }
}
