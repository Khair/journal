package ru.itis.models;
/*
* @author Rustem Khairutdinov
* @group 11-602
*/

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="\"group\"")
public class Group implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name; // имя

    @OneToMany(
            fetch = FetchType.LAZY,
            mappedBy = "group",
            cascade = CascadeType.ALL
    )
    private List<Student> students = new ArrayList<>();

    @ManyToOne(
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL
    )
    @JoinColumn(name = "course_id")
    private Course course;

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "institution_id")
    private Institution institution;

    @ManyToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "teacher_id")
    private List<Teacher> teachers = new ArrayList<>();

    public Group() {

    }

    public Group(String name, List<Student> students, Course course, Institution institution, List<Teacher> teachers) {
        this.name = name;
        this.students = students;
        this.course = course;
        this.institution = institution;
        this.teachers = teachers;
    }

    public Long getId() {
        return id;
    }

    public Group setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Group setName(String name) {
        this.name = name;
        return this;
    }

    public List<Student> getStudents() {
        return students;
    }

    public Group setStudents(List<Student> students) {
        this.students = students;
        return this;
    }

    public Course getCourse() {
        return course;
    }

    public Group setCourse(Course course) {
        this.course = course;
        return this;
    }

    public Institution getInstitution() {
        return institution;
    }

    public Group setInstitution(Institution institution) {
        this.institution = institution;
        return this;
    }

    public List<Teacher> getTeachers() {
        return teachers;
    }

    public Group setTeachers(List<Teacher> teachers) {
        this.teachers = teachers;
        return this;
    }
}
