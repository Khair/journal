package ru.itis.models;
/*
* @author Rustem Khairutdinov 
* @group 11-602
*/

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="\"institution\"")
public class Institution implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name; // имя

    @OneToMany(
            fetch = FetchType.LAZY,
            mappedBy = "institution",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Group> groups = new ArrayList<>();

    @OneToMany(
            fetch = FetchType.LAZY,
            mappedBy = "institution",
            cascade = CascadeType.ALL
    )
    private List<Subject> subjects = new ArrayList<>();

    public Institution() {

    }

    public Institution(String name, List<Group> groups, List<Subject> subjects) {
        this.name = name;
        this.groups = groups;
        this.subjects = subjects;
    }

    public Long getId() {
        return id;
    }

    public Institution setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Institution setName(String name) {
        this.name = name;
        return this;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public Institution setGroups(List<Group> groups) {
        this.groups = groups;
        return this;
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public Institution setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
        return this;
    }
}
