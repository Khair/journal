package ru.itis.models;
/*
* @author Rustem Khairutdinov
* @group 11-602
*/

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="\"student\"")
public class Student implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String firstName; // имя
    private String secondName; // фамилия
    private String fatherName; // отчество
    private String birthday; // дата рождения
    private String citizenship; // гражданство
    private String birthPlace; // город рождения

    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "group_id")
    private Group group;

    @OneToMany(
            fetch = FetchType.LAZY,
            mappedBy = "student",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Points> points = new ArrayList<>();

    @OneToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private User user;

    public List<Letter> getLetters() {
        return letters;
    }

    public Student setLetters(List<Letter> letters) {
        this.letters = letters;
        return this;
    }

    @OneToMany(
            fetch = FetchType.LAZY,
            mappedBy = "student",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Letter> letters = new ArrayList<>();

    public Student() {}

    public Student(String firstName, String secondName, String fatherName, String birthday, String citizenship,
                   String birthPlace, Group group, List<Points> points, User user, List<Letter> letters) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.fatherName = fatherName;
        this.birthday = birthday;
        this.citizenship = citizenship;
        this.birthPlace = birthPlace;
        this.group = group;
        this.points = points;
        this.user = user;
        this.letters = letters;
    }

    public Long getId() {
        return id;
    }

    public Student setId(Long id) {
        this.id = id;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public Student setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getSecondName() {
        return secondName;
    }

    public Student setSecondName(String secondName) {
        this.secondName = secondName;
        return this;
    }

    public String getFatherName() {
        return fatherName;
    }

    public Student setFatherName(String fatherName) {
        this.fatherName = fatherName;
        return this;
    }

    public String getBirthday() {
        return birthday;
    }

    public Student setBirthday(String birthday) {
        this.birthday = birthday;
        return this;
    }

    public String getCitizenship() {
        return citizenship;
    }

    public Student setCitizenship(String citizenship) {
        this.citizenship = citizenship;
        return this;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public Student setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
        return this;
    }

    public Group getGroup() {
        return group;
    }

    public Student setGroup(Group group) {
        this.group = group;
        return this;
    }

    public List<Points> getPoints() {
        return points;
    }

    public Student setPoints(List<Points> points) {
        this.points = points;
        return this;
    }

    public User getUser() {
        return user;
    }

    public Student setUser(User user) {
        this.user = user;
        return this;
    }

    @Override
    public String toString() {
        return String.format(
                "Student[id=%d, firstName='%s', lastName='%s', fatherName='%s']",
                id, firstName, secondName, fatherName);
    }
}
