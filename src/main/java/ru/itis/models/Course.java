package ru.itis.models;
/*
* @author Rustem Khairutdinov
* @group 11-602
*/

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="\"course\"")
public class Course implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;

    @OneToMany(
            fetch = FetchType.LAZY,
            mappedBy = "course",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Group> groups = new ArrayList<>();

    @ManyToMany(
            fetch = FetchType.LAZY,
            mappedBy = "courses",
            cascade = CascadeType.ALL
    )
    private List<Subject> subjects = new ArrayList<>();

    public Course() {

    }

    public Course(String name, List<Group> groups, List<Subject> subjects) {
        this.name = name;
        this.groups = groups;
        this.subjects = subjects;
    }

    public Long getId() {
        return id;
    }

    public Course setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Course setName(String name) {
        this.name = name;
        return this;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public Course setGroups(List<Group> groups) {
        this.groups = groups;
        return this;
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public Course setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
        return this;
    }
}
