package ru.itis.models;
/*
* @author Rustem Khairutdinov
* @group 11-602
*/

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="\"teacher\"")
public class Teacher implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String firstName; // имя
    private String secondName; // фамилия
    private String fatherName; // отчество
    private String birthday; // дата рождения
    private String citizenship; // гражданство
    private String birthPlace; // город рождения

    @ManyToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL,mappedBy = "teachers")
    private List<Group> groups = new ArrayList<>();

    @ManyToMany(fetch = FetchType.LAZY,cascade = CascadeType.ALL,mappedBy = "teachers")
    private List<Subject> subjects = new ArrayList<>();

    @OneToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private User user;

    public List<Points> getPoints() {
        return points;
    }

    public Teacher setPoints(List<Points> points) {
        this.points = points;
        return this;
    }

    public Teacher(String firstName, String secondName, String fatherName, String birthday, String citizenship, String birthPlace, List<Group> groups, List<Subject> subjects, User user, List<Points> points) {

        this.firstName = firstName;
        this.secondName = secondName;
        this.fatherName = fatherName;
        this.birthday = birthday;
        this.citizenship = citizenship;
        this.birthPlace = birthPlace;
        this.groups = groups;
        this.subjects = subjects;
        this.user = user;
        this.points = points;
    }

    @OneToMany(
            fetch = FetchType.LAZY,
            mappedBy = "teacher",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Points> points = new ArrayList<>();

    @OneToMany(
            fetch = FetchType.LAZY,
            mappedBy = "teacher",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Letter> letters = new ArrayList<>();

    public Teacher() {

    }

    public Teacher(String firstName, String secondName, String fatherName, String birthday, String citizenship, String birthPlace, List<Group> groups, List<Subject> subjects, User user) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.fatherName = fatherName;
        this.birthday = birthday;
        this.citizenship = citizenship;
        this.birthPlace = birthPlace;
        this.groups = groups;
        this.subjects = subjects;
        this.user = user;
    }

    public Teacher(String firstName, String secondName, String fatherName, String birthday, String citizenship, String birthPlace, List<Group> groups, List<Subject> subjects, User user, List<Points> points, List<Letter> letters) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.fatherName = fatherName;
        this.birthday = birthday;
        this.citizenship = citizenship;
        this.birthPlace = birthPlace;
        this.groups = groups;
        this.subjects = subjects;
        this.user = user;
        this.points = points;
        this.letters = letters;
    }

    public List<Letter> getLetters() {
        return letters;
    }

    public Teacher setLetters(List<Letter> letters) {
        this.letters = letters;
        return this;
    }

    public Long getId() {
        return id;
    }

    public Teacher setId(Long id) {
        this.id = id;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public Teacher setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getSecondName() {
        return secondName;
    }

    public Teacher setSecondName(String secondName) {
        this.secondName = secondName;
        return this;
    }

    public String getFatherName() {
        return fatherName;
    }

    public Teacher setFatherName(String fatherName) {
        this.fatherName = fatherName;
        return this;
    }

    public String getBirthday() {
        return birthday;
    }

    public Teacher setBirthday(String birthday) {
        this.birthday = birthday;
        return this;
    }

    public String getCitizenship() {
        return citizenship;
    }

    public Teacher setCitizenship(String citizenship) {
        this.citizenship = citizenship;
        return this;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public Teacher setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
        return this;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public Teacher setGroups(List<Group> groups) {
        this.groups = groups;
        return this;
    }

    public List<Subject> getSubjects() {
        return subjects;
    }

    public Teacher setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
        return this;
    }

    public User getUser() {
        return user;
    }

    public Teacher setUser(User user) {
        this.user = user;
        return this;
    }
}
