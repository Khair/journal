package ru.itis.dto;
/*
* @author Rustem Khairutdinov 
* @group 11-602
*/

public class SubjectCheckDto {
    private String subjectid;
    private String name;
    private Boolean checked;

    public String getSubjectid() {
        return subjectid;
    }

    public void setSubjectid(String subjectid) {
        this.subjectid = subjectid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    public SubjectCheckDto() {
    }

    public SubjectCheckDto(String subjectid, String name, Boolean checked) {

        this.subjectid = subjectid;
        this.name = name;
        this.checked = checked;
    }
}
