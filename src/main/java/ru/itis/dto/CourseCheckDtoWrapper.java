package ru.itis.dto;
/*
* @author Rustem Khairutdinov 
* @group 11-602
*/

import java.util.ArrayList;
import java.util.List;

public class CourseCheckDtoWrapper {
    List<CourseCheckDto> courseCheckDtoList = new ArrayList<>();

    public CourseCheckDtoWrapper() {
    }

    public CourseCheckDtoWrapper(List<CourseCheckDto> courseCheckDtoList) {
        this.courseCheckDtoList = courseCheckDtoList;
    }

    public List<CourseCheckDto> getCourseCheckDtoList() {
        return courseCheckDtoList;
    }

    public void setCourseCheckDtoList(List<CourseCheckDto> courseCheckDtoList) {
        this.courseCheckDtoList = courseCheckDtoList;
    }
}
