package ru.itis.dto.validators;
/*
* @author Rustem Khairutdinov 
* @group 11-602
*/

import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ru.itis.dto.StudentDto;

@Component
public class StudentDtoValidator implements Validator {


    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.getName().equals(StudentDto.class.getName());
    }

    @Override
    public void validate(@Nullable Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "empty.firstName", "Please enter first name");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "secondName", "empty.secondName", "Please enter second name");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "fatherName", "empty.fatherName", "Please enter father name");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "birthday", "empty.birthday", "Please enter birthday");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "citizenship", "empty.citizenship", "Please enter citizenship");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "birthCity", "empty.birthCity", "Please enter birth city");
    }
}
