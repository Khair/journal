package ru.itis.dto;
/*
* @author Rustem Khairutdinov 
* @group 11-602
*/

import java.util.ArrayList;
import java.util.List;

public class GroupAndSubjectWrapper {
    private String firstname; // имя
    private String secondname; // фамилия
    private String fathername; // отчество
    private String birthday; // дата рождения
    private String citizenship; // гражданство
    private String birthplace;

    List<GroupCheckDto> groupCheckDtos = new ArrayList<>();
    List<SubjectCheckDto> subjectCheckDtos = new ArrayList<>();

    public GroupAndSubjectWrapper(String firstName, String secondName, String fatherName, String birthday, String citizenship, String birthPlace, List<GroupCheckDto> groupCheckDtos, List<SubjectCheckDto> subjectCheckDtos) {
        this.firstname = firstName;
        this.secondname = secondName;
        this.fathername = fatherName;
        this.birthday = birthday;
        this.citizenship = citizenship;
        this.birthplace = birthPlace;
        this.groupCheckDtos = groupCheckDtos;
        this.subjectCheckDtos = subjectCheckDtos;
    }

    public GroupAndSubjectWrapper(List<GroupCheckDto> groupCheckDtos, List<SubjectCheckDto> subjectCheckDtos){
        this.groupCheckDtos = groupCheckDtos;
        this.subjectCheckDtos = subjectCheckDtos;
    }

    public GroupAndSubjectWrapper() {
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstName) {
        this.firstname = firstName;
    }

    public String getSecondname() {
        return secondname;
    }

    public void setSecondname(String secondName) {
        this.secondname = secondName;
    }

    public String getFathername() {
        return fathername;
    }

    public void setFathername(String fatherName) {
        this.fathername = fatherName;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getCitizenship() {
        return citizenship;
    }

    public void setCitizenship(String citizenship) {
        this.citizenship = citizenship;
    }

    public String getBirthplace() {
        return birthplace;
    }

    public void setBirthplace(String birthPlace) {
        this.birthplace = birthPlace;
    }

    public List<GroupCheckDto> getGroupCheckDtos() {
        return groupCheckDtos;
    }

    public void setGroupCheckDtos(List<GroupCheckDto> groupCheckDtos) {
        this.groupCheckDtos = groupCheckDtos;
    }

    public List<SubjectCheckDto> getSubjectCheckDtos() {
        return subjectCheckDtos;
    }

    public void setSubjectCheckDtos(List<SubjectCheckDto> subjectCheckDtos) {
        this.subjectCheckDtos = subjectCheckDtos;
    }
}
