package ru.itis.dto;
/*
* @author Rustem Khairutdinov 
* @group 11-602
*/

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class StudentDto {
    @NotNull
    @NotEmpty
    private String firstName;
    @NotNull
    @NotEmpty
    private String secondName;
    private String fatherName;
    @NotNull
    @NotEmpty
    private String birthday;
    @NotNull
    @NotEmpty
    private String citizenship; // гражданство
    @NotNull
    @NotEmpty
    private String birthCity;
    @NotNull
    @NotEmpty
    private String groupId;
    @NotNull
    @NotEmpty
    private String institutionId;

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(String institutionId) {
        this.institutionId = institutionId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getCitizenship() {
        return citizenship;
    }

    public void setCitizenship(String citizenship) {
        this.citizenship = citizenship;
    }

    public String getBirthCity() {
        return birthCity;
    }

    public void setBirthCity(String birthCity) {
        this.birthCity = birthCity;
    }

    public StudentDto() {

    }

    public StudentDto(String firstName, String secondName, String fatherName, String birthday, String citizenship,
                      String birthCity, String institutionId, String groupId) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.fatherName = fatherName;
        this.birthday = birthday;
        this.citizenship = citizenship;
        this.birthCity = birthCity;
        this.institutionId = institutionId;
        this.groupId = groupId;
    }
}
