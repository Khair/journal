package ru.itis.dto;
/*
* @author Rustem Khairutdinov 
* @group 11-602
*/

public class GroupCheckDto {
    private String groupid;
    private String name;
    private Boolean checked;

    public String getGroupid() {
        return groupid;
    }

    public void setGroupid(String groupid) {
        this.groupid = groupid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    public GroupCheckDto() {

    }

    public GroupCheckDto(String groupid, String name, Boolean checked) {

        this.groupid = groupid;
        this.name = name;
        this.checked = checked;
    }
}
