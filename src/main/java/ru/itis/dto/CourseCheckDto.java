package ru.itis.dto;
/*
* @author Rustem Khairutdinov 
* @group 11-602
*/

public class CourseCheckDto {
    private String courseid;
    private String name;
    private Boolean checked;

    public CourseCheckDto() {
    }

    public CourseCheckDto(String courseid, Boolean checked, String name) {
        this.courseid = courseid;
        this.checked = checked;
        this.name = name;
    }

    public String getCourseid() {
        return courseid;
    }

    public void setCourseid(String courseid) {
        this.courseid = courseid;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
