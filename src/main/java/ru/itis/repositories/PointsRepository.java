package ru.itis.repositories;
/*
* @author Rustem Khairutdinov
* @group 11-602
*/

import ru.itis.models.Points;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PointsRepository extends JpaRepository<Points, Long> {
}
