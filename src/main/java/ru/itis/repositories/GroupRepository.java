package ru.itis.repositories;
/*
* @author Rustem Khairutdinov
* @group 11-602
*/

import ru.itis.models.Group;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupRepository extends JpaRepository<Group, Long> {
}
