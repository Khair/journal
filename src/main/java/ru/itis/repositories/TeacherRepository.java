package ru.itis.repositories;
/*
* @author Rustem Khairutdinov 
* @group 11-602
*/

import ru.itis.models.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TeacherRepository extends JpaRepository<Teacher, Long>{

    Optional<Teacher> findByUser_Id(Long userId);
}
