package ru.itis.repositories;
/*
* @author Rustem Khairutdinov
* @group 11-602
*/

import ru.itis.models.Institution;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface InstitutionRepository extends JpaRepository<Institution, Long> {

    Optional<Institution> findFirstByName(String name);

    @Override
    Optional<Institution> findById(Long aLong);
}
