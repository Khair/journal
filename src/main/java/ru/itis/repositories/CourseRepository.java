package ru.itis.repositories;
/*
* @author Rustem Khairutdinov
* @group 11-602
*/

import ru.itis.models.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CourseRepository extends JpaRepository<Course, Long>{

    Optional<Course> findFirstByName(String name);
}
