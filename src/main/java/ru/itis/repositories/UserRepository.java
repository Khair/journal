package ru.itis.repositories;
/*
* @author Rustem Khairutdinov 
* @group 11-602
*/

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import ru.itis.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{

    Optional<User> findUserByLogin(String login);
    Optional<User> findOneByLogin(String login);
    @Transactional
    @Modifying
    @Query("update User u set u.password = ?1 where u.id = ?2")
    void setUserPasswordById(String password, Long userId);
}
