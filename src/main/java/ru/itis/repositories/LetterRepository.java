package ru.itis.repositories;
/*
* @author Rustem Khairutdinov
* @group 11-602
*/

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.models.Letter;

public interface LetterRepository extends JpaRepository<Letter, Long> {
}
