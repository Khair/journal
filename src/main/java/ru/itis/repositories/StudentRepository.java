package ru.itis.repositories;
/*
* @author Rustem Khairutdinov 
* @group 11-602
*/

import ru.itis.models.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long>{

    Optional<Student> findByUser_Id(Long userId);
}
