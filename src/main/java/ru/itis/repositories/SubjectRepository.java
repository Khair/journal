package ru.itis.repositories;
/*
* @author Rustem Khairutdinov
* @group 11-602
*/

import ru.itis.models.Subject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SubjectRepository extends JpaRepository<Subject, Long> {
    Optional<Subject> findFirstByName(String name);

    Optional<Subject> findFirstByNameAndInstitution_Id(String name, Long id);

    Optional<Subject> findFirstByIdAndInstitution_Id(Long id, Long institutionId);

    Optional<Subject> findSubjectByIdAndInstitution_Id(Long id, Long institution_Id);


}
