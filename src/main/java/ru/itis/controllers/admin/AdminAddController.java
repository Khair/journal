package ru.itis.controllers.admin;
/*
* @author Rustem Khairutdinov 
* @group 11-602
*/

import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.itis.dto.CourseCheckDtoWrapper;
import ru.itis.dto.GroupAndSubjectWrapper;
import ru.itis.dto.StudentDto;
import ru.itis.dto.validators.StudentDtoValidator;
import ru.itis.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import ru.itis.services.admin.AdminAddService;

import javax.validation.Valid;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/admin/add")
public class AdminAddController {

    @Autowired
    private AdminAddService adminAddService;

    @Autowired
    private StudentDtoValidator studentDtoValidator;

    @InitBinder("addStudent")
    public void initUserFormValidator(WebDataBinder binder){
        binder.addValidators(studentDtoValidator);
    }

    @GetMapping("/institution")
    public String getInstitutionPage(ModelMap modelMap) {
        boolean isEmpty = false;
        List<Institution> institutions = new ArrayList<>();
        try {
            institutions = adminAddService.getInstitutions();
        }catch (Exception e){
            isEmpty = true;
        }
        if(!isEmpty) {
            institutions.sort(Comparator.comparing(Institution::getName));
            modelMap.addAttribute("institutions", institutions);
        }
        return "admin_add_institution";
    }

    @PostMapping("/institution")
    public String postInstitution(@RequestParam String name, RedirectAttributes redirectAttributes) {
        boolean isSaved = true;
        try {
            adminAddService.saveInstitution(name);
        }catch (Exception e){
            isSaved = false;
            redirectAttributes.addFlashAttribute("error", e.getMessage());
        }
        if(isSaved) redirectAttributes.addFlashAttribute("message", "Added successfully");
        return "redirect:institution";
    }

    @GetMapping("/subject")
    public String getSubjectPage(ModelMap model){
        boolean isEmpty = false;
        List<Institution> institutions = new ArrayList<>();
        try {
            institutions = adminAddService.getInstitutions();
        }catch (Exception e){
            model.addAttribute("error",e.getMessage());
            isEmpty = true;
        }
        if(!isEmpty) {
            institutions.sort(Comparator.comparing(Institution::getName));
            model.addAttribute("institutions", institutions);
        }

        return "admin_add_subject";
    }

    @GetMapping("/subject{institution-id}")
    public String getSubjectPageByInstitution(@PathVariable("institution-id") String institutionId, ModelMap model){
        boolean isEmpty = false;
        Institution institution = null;
        List<Course> courses = new ArrayList<>();
        List<Subject> subjects = new ArrayList<>();
        try {
            institution = adminAddService.getInstitutionById(institutionId);
            courses = adminAddService.getCourses();
            subjects = institution.getSubjects();
        }catch (Exception e){
            model.addAttribute("error",e.getMessage());
            isEmpty = true;
        }
        if(!isEmpty) {
            CourseCheckDtoWrapper courseCheckDtoWrapper = adminAddService.getCourseCheckDtoWrapper(courses);
            model.addAttribute("institution", institution);
            model.addAttribute("courseCheckDtoWrapper", courseCheckDtoWrapper);
            model.addAttribute("subjects", subjects);
        }
        return "admin_add_subject_for_inst";
    }

    @PostMapping("/subject")
    public String postSubject(@RequestParam String subjectName,@RequestParam String institutionId,
                              @ModelAttribute CourseCheckDtoWrapper courseCheckDtoWrapper,
                              RedirectAttributes redirectAttributes){
        boolean isSaved = true;
        try {
            Institution institution = adminAddService.getInstitutionById(institutionId);
            adminAddService.saveSubject(subjectName, courseCheckDtoWrapper, institution);
        }catch (Exception e){
            isSaved = false;
            redirectAttributes.addFlashAttribute("error", e.getMessage());
        }
        if(isSaved) {
            redirectAttributes.addFlashAttribute("message", "Added successfully");
        }
        return "redirect:subject" + institutionId;
    }

    @GetMapping("/group")
    public String getGroupPage(ModelMap model){
        boolean isEmpty = false;
        List<Institution> institutions = new ArrayList<>();
        List<Course> courses = new ArrayList<>();
        try {
            institutions = adminAddService.getInstitutions();
            courses = adminAddService.getCourses();
        }catch (Exception e){
            model.addAttribute("error",e.getMessage());
            isEmpty = true;
        }
        if(!isEmpty) {
            model.addAttribute("institutions", institutions);
            model.addAttribute("courses", courses);
        }
        return "admin_add_groups";
    }

    @GetMapping("/group{institution-id}-{course-id}")
    public String getGroupPageByInstitution(@PathVariable("course-id") String courseId,
                                            @PathVariable("institution-id") String institutionId,
                                            ModelMap model){
        boolean isEmpty = false;
        Institution institution = null;
        Course course = null;
        List<Group> groups = new ArrayList<>();
        try {
            institution = adminAddService.getInstitutionById(institutionId);
            course = adminAddService.getCourseById(courseId);
            groups = institution.getGroups();
        }catch (Exception e){
            model.addAttribute("error",e.getMessage());
            isEmpty = true;
        }
        if(!isEmpty) {
            model.addAttribute("institution", institution);
            model.addAttribute("course", course);
            model.addAttribute("groups", groups);
        }
        return "admin_add_group_for_inst";
    }

    @PostMapping("/group")
    public String postGroup(RedirectAttributes redirectAttributes, @RequestParam String name,
                            @RequestParam String courseId, @RequestParam String institutionId){
        boolean isSaved = true;
        try {
            Institution institution = adminAddService.getInstitutionById(institutionId);
            Course course = adminAddService.getCourseById(courseId);
            adminAddService.saveGroup(name,course,institution);
        }catch (Exception e){
            isSaved = false;
            redirectAttributes.addFlashAttribute("error", e.getMessage());
        }
        if(isSaved) redirectAttributes.addFlashAttribute("message", "Added successfully");
        return "redirect:group" + institutionId + "-" + courseId;
    }

    @GetMapping("/student")
    public String getStudentPage(ModelMap modelMap){
        boolean isEmpty = false;
        List<Institution> institutions = new ArrayList<>();
        try {
            institutions = adminAddService.getInstitutions();
        }catch (Exception e){
            modelMap.addAttribute("error",e.getMessage());
            isEmpty = true;
        }
        StudentDto studentDto = new StudentDto();
        if(!isEmpty) {
            institutions.sort(Comparator.comparing(Institution::getName));
            modelMap.addAttribute("institutions",institutions);
        }
        return "admin_add_student";
    }

    @GetMapping("/student{institution-id}-{group-id}")
    public String getStudentPageForInstitute(@PathVariable("group-id") String groupId,
                                             @PathVariable("institution-id") String institutionId,
                                             ModelMap modelMap){
        boolean isEmpty = false;
        Group group = null;
        Institution institution = null;
        List<Student> students = new ArrayList<>();
        try {
            institution = adminAddService.getInstitutionById(institutionId);
            group = adminAddService.getGroupById(groupId);
            students = group.getStudents();
        }catch (Exception e){
            modelMap.addAttribute("error",e.getMessage());
            isEmpty = true;
        }
        if(!isEmpty) {
            modelMap.addAttribute("group", group);
            modelMap.addAttribute("students", students);
            modelMap.addAttribute("institution", institution);
        }
        return "admin_add_student_for_inst_and_group";
    }

    @PostMapping("/student")
    public String postStudent(@Valid @ModelAttribute("addStudent") StudentDto studentDto,
                              BindingResult bindingResult,
                              RedirectAttributes redirectAttributes){
        boolean isSaved = true;
        if (bindingResult.hasErrors()){
            redirectAttributes.addFlashAttribute("error", "Contain all input fields please");
            return "redirect:student" + studentDto.getInstitutionId() + "-" + studentDto.getGroupId();
        }
        try {
            Group group = adminAddService.getGroupById(studentDto.getGroupId());
            adminAddService.saveStudent(studentDto,group);
        }catch (Exception e){
            isSaved = false;
            redirectAttributes.addFlashAttribute("error", e.getMessage());
        }
        if(isSaved) redirectAttributes.addFlashAttribute("message", "Added successfully");
        return "redirect:student" + studentDto.getInstitutionId() + "-" + studentDto.getGroupId();
    }

    @GetMapping("/teacher")
    public String getTeacherPage(ModelMap model){
        boolean isEmpty = false;
        List<Institution> institutions = new ArrayList<>();
        try {
            institutions = adminAddService.getInstitutions();
        }catch (Exception e){
            model.addAttribute("error",e.getMessage());
            isEmpty = true;
        }
        if(!isEmpty) {
            institutions.sort(Comparator.comparing(Institution::getName));
            model.addAttribute("institutions",institutions);
        }
        return "add_teacher";
    }

    @GetMapping("/teacher{institution-id}")
    public String getTeacherPageForInstitutes(@PathVariable("institution-id") String institutionId, ModelMap modelMap){
        boolean isEmpty = false;
        Institution institution = null;
        List<Group> groups = new ArrayList<>();
        List<Subject> subjects = new ArrayList<>();
        List<Teacher> teachers = new ArrayList<>();
        try {
            institution = adminAddService.getInstitutionById(institutionId);
            teachers = institution.getSubjects().stream()
                    .map(Subject::getTeachers)
                    .flatMap(Collection::stream)
                    .distinct()
                    .collect(Collectors.toList());
            groups = adminAddService.getGroups();
            subjects = institution.getSubjects();
        }catch (Exception e){
            modelMap.addAttribute("error",e.getMessage());
            isEmpty = true;
        }
        if(!isEmpty) {
            GroupAndSubjectWrapper groupAndSubjectWrapper = adminAddService.getGroupAndSubjectWrapper(groups, subjects);
            modelMap.addAttribute("institution", institution);
            modelMap.addAttribute("teachers", teachers);
            modelMap.addAttribute("groups", groups);
            modelMap.addAttribute("groupAndSubjectWrapper", groupAndSubjectWrapper);
        }
        return "admin_add_teacher_for_inst";
    }

    @PostMapping("/teacher")
    public String postTeacher(@RequestParam String institutionId,
                              @ModelAttribute GroupAndSubjectWrapper groupAndSubjectWrapper,
                              RedirectAttributes redirectAttributes){
        boolean isSaved = true;
        try {
            adminAddService.saveTeacher(groupAndSubjectWrapper);
        }catch (Exception e){
            isSaved = false;
            redirectAttributes.addFlashAttribute("error", e.getMessage());
        }
        if(isSaved) {
            redirectAttributes.addFlashAttribute("message", "Added successfully");
        }
        return "redirect:teacher" + institutionId;
    }

//    @GetMapping("/course")
//    public String addCoursePage(){
//        return "add_course";
//    }

//    @PostMapping("/course")
//    public String addCourse(){
//        return "add_course";
//    }
}
