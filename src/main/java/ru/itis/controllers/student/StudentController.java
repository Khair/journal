package ru.itis.controllers.student;
/*
* @author Rustem Khairutdinov 
* @group 11-602
*/

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.itis.models.*;
import ru.itis.services.AuthenticationService;
import ru.itis.services.student.StudentService;

import javax.websocket.server.PathParam;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/student")
public class StudentController {

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private StudentService studentService;

    @GetMapping("/")
    public String getPoints(Authentication authentication, ModelMap modelMap){
        User user = authenticationService.getUserByAuthentication(authentication);
        Student student = studentService.getStudentByUserId(user.getId());
        List<Subject> allSubjects = studentService.getSubjects(student);
        List<Points> pointsList = student.getPoints();
        Map<Subject, List<Points>> map = new HashMap<>();
        for(int i = 0; i < allSubjects.size(); i++){
            map.put(allSubjects.get(i), new ArrayList<>());
        }
        for(int i = 0; i < pointsList.size(); i++){
            List<Points> list = map.get(pointsList.get(i).getSubject());
            list.add(pointsList.get(i));
            map.put(pointsList.get(i).getSubject(),list);
        }
        modelMap.addAttribute("mapk", map);
        return "student";
    }

    @GetMapping("/mailto")
    public String getMailTo(Authentication authentication, ModelMap modelMap){
        User user = authenticationService.getUserByAuthentication(authentication);
        Student student = studentService.getStudentByUserId(user.getId());
        List<Teacher> teachers = student.getGroup().getTeachers();
        modelMap.addAttribute("teachers", teachers);
        return "student_mail_to";
    }

    @GetMapping("/mailto{teacher-id}")
    public String getMailToTeachers(ModelMap modelMap, @PathVariable("teacher-id") String teacherId){
        Teacher teacher = null;
        try {
            teacher = studentService.getTeacherById(teacherId);
        } catch (Exception e) {
            modelMap.addAttribute("error", e.getMessage());
        }
        modelMap.addAttribute("teacher", teacher);
        return "student_mail_to_teacher";
    }

    @PostMapping("/mailto")
    public String postMailToTeacher(RedirectAttributes redirectAttributes, @RequestParam String letter,
                                    @RequestParam String theme, @RequestParam String teacherId,
                                    Authentication authentication){
        User user = authenticationService.getUserByAuthentication(authentication);
        Student student = studentService.getStudentByUserId(user.getId());
        Teacher teacher = null;
        try {
            teacher = studentService.getTeacherById(teacherId);
            if(letter.matches("[ ]*") && theme.matches("[ ]*")){
                throw new Exception("Write letter and theme");
            }
        } catch (Exception e) {
            redirectAttributes.addFlashAttribute("error", e.getMessage());
            return "redirect:mailto" + teacherId;
        }
        studentService.saveLetter(teacher, theme, letter, student);
        redirectAttributes.addFlashAttribute("teacher", teacher);
        return "redirect:mailto";
    }

    @GetMapping("/mailfrom")
    public String getMailFrom(Authentication authentication, ModelMap modelMap){
        User user = authenticationService.getUserByAuthentication(authentication);
        Student student = studentService.getStudentByUserId(user.getId());
        List<Letter> letters = student.getLetters()
                .stream()
                .filter(letter -> !letter.getFromStudent())
                .collect(Collectors.toList());
        modelMap.addAttribute("letters", letters);
        return "student_mail_from";
    }
}
