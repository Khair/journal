package ru.itis.controllers.teacher;
/*
* @author Rustem Khairutdinov 
* @group 11-602
*/

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.itis.models.*;
import ru.itis.services.AuthenticationService;
import ru.itis.services.teacher.TeacherService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/teacher")
public class TeacherController {

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private TeacherService teacherService;

    @GetMapping("/")
    public String getAdminPage() {
        return "teacher";
    }

    @GetMapping("/subject")
    public String getSubjectsPage(Authentication authentication, ModelMap modelMap) {
        User user = authenticationService.getUserByAuthentication(authentication);
        Teacher teacher = teacherService.getTeacherByUserId(user.getId());
        List<Subject> subjects = teacher.getSubjects();
        modelMap.addAttribute("subjects", subjects);
        return "teacher_subjects";
    }

    @GetMapping("/subject{subject_id}")
    public String getSubjectById(Authentication authentication, ModelMap modelMap, @PathVariable("subject_id") String subjectId) {
        User user = authenticationService.getUserByAuthentication(authentication);
        Teacher teacher = teacherService.getTeacherByUserId(user.getId());
        Subject subject = null;
        List<Group> groups = new ArrayList<>();
        try {
            subject = teacherService.getSubjectById(subjectId);
            Institution institution = subject.getInstitution();
            List<Course> course = subject.getCourses();

            groups= teacherService.getGroups(institution, course, teacher);
        } catch (Exception e) {
            e.printStackTrace();
        }
        modelMap.addAttribute("subject", subject);
        modelMap.addAttribute("groups", groups);
        return "teacher_subject_group";
    }

    @GetMapping("/subject{subject_id}/group{group_id}")
    public String getConcreteGroup(Authentication authentication, ModelMap modelMap, @PathVariable("subject_id") String subjectId,
                                  @PathVariable("group_id") String groupId) {
        User user = authenticationService.getUserByAuthentication(authentication);
        Teacher teacher = teacherService.getTeacherByUserId(user.getId());
        List<Student> students = new ArrayList<>();
        Group group = null;
        try {
            group = teacherService.getGroupById(groupId);
            students = group.getStudents();
        } catch (Exception e) {
            e.printStackTrace();
        }
        modelMap.addAttribute("students",students);
        modelMap.addAttribute("subjectId", subjectId);
        modelMap.addAttribute("group", group);
        return "teacher_subject_group_students";
    }

    @GetMapping("/subject{subject_id}/group{group_id}/student{student_id}")
    public String getConcreteStudent(Authentication authentication, ModelMap modelMap, @PathVariable("subject_id") String subjectId,
                                  @PathVariable("group_id") String groupId, @PathVariable("student_id") String studentId) {
        User user = authenticationService.getUserByAuthentication(authentication);
        Teacher teacher = teacherService.getTeacherByUserId(user.getId());
        List<Points> points = new ArrayList<>();
        Student student = null;
        Subject subject = null;
        try {
            student = teacherService.getStudentById(studentId);
            points = student.getPoints();
            subject = teacherService.getSubjectById(subjectId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        modelMap.addAttribute("subjectName", subject);
        modelMap.addAttribute("points", points);
        modelMap.addAttribute("student", student);
        modelMap.addAttribute("subjectId", subjectId);
        modelMap.addAttribute("groupId", groupId);
        return "teacher_concrete_student";
    }

    @PostMapping("/points")
    public String postPointsToStudent(@RequestParam String points, @RequestParam String subjectId,
                                      @RequestParam String studentId, @RequestParam String comment,
                                      @RequestParam String groupId,
                                      RedirectAttributes modelMap,
                                      Authentication authentication){
        User user = authenticationService.getUserByAuthentication(authentication);
        Teacher teacher = teacherService.getTeacherByUserId(user.getId());
        Student student = null;
        Subject subject = null;
        boolean isSaved = true;
        try {
            student = teacherService.getStudentById(studentId);
            subject = teacherService.getSubjectById(subjectId);
            teacherService.savePoints(teacher, points, subject, student, comment);
        } catch (Exception e) {
            e.printStackTrace();
            modelMap.addFlashAttribute("error", e.getMessage());
            isSaved = false;
        }
        if(isSaved){
            modelMap.addFlashAttribute("message", "Added successfully");
        }
        return "redirect:subject" + subjectId + "/groups" + groupId + "/student" + studentId;
    }

    @GetMapping("/mailto")
    public String getMailTo(Authentication authentication, ModelMap modelMap){
        User user = authenticationService.getUserByAuthentication(authentication);
        Teacher teacher = teacherService.getTeacherByUserId(user.getId());
        List<Student> students = teacher.getGroups().stream()
                .map(Group::getStudents)
                .flatMap(Collection::stream)
                .distinct()
                .collect(Collectors.toList());

        modelMap.addAttribute("students", students);
        return "teacher_mail_to";
    }

    @GetMapping("/mailto{student-id}")
    public String getMailToStudents(ModelMap modelMap, @PathVariable("student-id") String studentId){
        Student student = null;
        try {
            student = teacherService.getStudentById(studentId);
        } catch (Exception e) {
            modelMap.addAttribute("error", e.getMessage());
        }
        modelMap.addAttribute("student", student);
        return "teacher_mail_to_teacher";
    }

    @PostMapping("/mailto")
    public String postMailToTeacher(RedirectAttributes redirectAttributes, @RequestParam String letter,
                                    @RequestParam String theme, @RequestParam String studentId,
                                    Authentication authentication){
        User user = authenticationService.getUserByAuthentication(authentication);
        Teacher teacher = teacherService.getTeacherByUserId(user.getId());
        Student student = null;
        try {
            student = teacherService.getStudentById(studentId);
            if(letter.matches("[ ]*") && theme.matches("[ ]*")){
                throw new Exception("Write letter and theme");
            }
        } catch (Exception e) {
            redirectAttributes.addFlashAttribute("error", e.getMessage());
            return "redirect:mailto" + studentId;
        }
        teacherService.saveLetter(teacher, theme, letter, student);
        redirectAttributes.addFlashAttribute("teacher", teacher);
        redirectAttributes.addFlashAttribute("message", "Your letter was sent");
        return "redirect:mailto";
    }

    @GetMapping("/mailfrom")
    public String getMailFrom(Authentication authentication, ModelMap modelMap){
        User user = authenticationService.getUserByAuthentication(authentication);
        Teacher teacher = teacherService.getTeacherByUserId(user.getId());
        List<Letter> letters = teacher.getLetters()
                .stream()
                .filter(Letter::getFromStudent)
                .collect(Collectors.toList());
        modelMap.addAttribute("letters", letters);
        return "teacher_mail_from";
    }
}
