package ru.itis.controllers;
/*
* @author Rustem Khairutdinov 
* @group 11-602
*/

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.itis.security.details.UserDetailsServiceImpl;
import ru.itis.security.roles.Role;
import ru.itis.services.AuthenticationService;

import javax.security.auth.login.FailedLoginException;
import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.Optional;

@Controller
public class LoginController {

    @Autowired
    private AuthenticationService authenticationService;

    @GetMapping("/")
    public String showLoginPage(Authentication authentication, @RequestParam Optional<String> error, Model model) {
        if(authentication != null) {
            final Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
            for (final GrantedAuthority grantedAuthority : authorities) {
                if (grantedAuthority.getAuthority().equals(Role.ADMIN.name())) {
                    return "redirect:/admin/";
                } else if (grantedAuthority.getAuthority().equals(Role.USER.name())) {
                    return "redirect:/student/";
                } else if (grantedAuthority.getAuthority().equals(Role.TEACHER.name())) {
                    return "redirect:/teacher/";
                } else {
                    authentication = null;
                    return "redirect:/";
                }
            }
        }
        error.ifPresent(s -> model.addAttribute("error", new FailedLoginException("Login or password is not correct")));
        return "loginpage";
    }

//    @GetMapping("/reg")
//    public String showRegistrationPage() {
//        return "regpage";
//    }

//    @PostMapping("/reg")
//    public String postRegistrationPage(@RequestParam("login")String login,@RequestParam("password")String password) {
//        userService.saveUser(login, password);
//        return "redirect:/";
//    }

    @GetMapping("/logout")
    public String logout(HttpServletRequest request, Authentication authentication) {
        if (authentication != null) {
            request.getSession().invalidate();
        }
        return "redirect:/";
    }

//    @GetMapping("/login")
//    public String showLoginPageParam() {
//        return "loginpage";
//    }
}
