package ru.itis.services.teacher;
/*
* @author Rustem Khairutdinov 
* @group 11-602
*/

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.models.*;
import ru.itis.repositories.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TeacherService {

    @Autowired
    private TeacherRepository teacherRepository;

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private InstitutionRepository institutionRepository;

    @Autowired
    private SubjectRepository subjectRepository;

    @Autowired
    private PointsRepository pointsRepository;

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private LetterRepository letterRepository;

    public Teacher getTeacherByUserId(Long id){
        return teacherRepository.findByUser_Id(id).get();
    }

    public Institution getInstitutionById(String institutionId) throws Exception {
        Optional<Institution> institution = institutionRepository.findById(Long.parseLong(institutionId));
        if(!institution.isPresent()){
            throw new Exception("does not exists, you should create them or choose correct");
        }
        return institution.get();
    }

    public Subject getSubjectById(String subjectId) throws Exception {
        Optional<Subject> subject = subjectRepository.findById(Long.parseLong(subjectId));
        if(!subject.isPresent()){
            throw new Exception("does not exists, you should create them or choose correct");
        }
        return subject.get();
    }

    public List<Group> getGroups(Institution institution, List<Course> course, Teacher teacher) {
        List<Group> groupsForInst = institution.getGroups();
        List<Group> groupsForCourses = course.stream()
                .flatMap(course1 -> course1.getGroups().stream())
                .collect(Collectors.toList());
        List<Group> groups = teacher.getGroups();

        List<Group> tempList = intersactionSubjects(groupsForInst, groupsForCourses);

        List<Group> result = intersactionSubjects(tempList, groups);
        return result;
    }

    private List<Group> intersactionSubjects(List<Group> subjects, List<Group> subjects2){
        List<Group> tempList = new ArrayList<>();

        for (int i = 0; i < subjects.size(); i++) {
            int count = 0;
            for (int j = 0; j < subjects2.size(); j++) {
                if(subjects.get(i).getId().equals(subjects2.get(j).getId()))
                    count = 1;
            }
            if(count > 0)
                tempList.add(subjects.get(i));
        }
        return tempList;
    }

    public Group getGroupById(String groupId) throws Exception {
        Optional<Group> group = groupRepository.findById(Long.parseLong(groupId));
        if(!group.isPresent()){
            throw new Exception("does not exists, you should create them or choose correct");
        }
        return group.get();
    }

    public Student getStudentById(String studentId) throws Exception {
        Optional<Student> student = studentRepository.findById(Long.parseLong(studentId));
        if(!student.isPresent()){
            throw new Exception("does not exists, you should create them or choose correct");
        }
        return student.get();
    }

    public void savePoints(Teacher teacher, String points, Subject subject, Student student, String comment) throws Exception {
        if(points == null || "".equals(points) || points.matches("[ ]*")){
            throw new Exception("Please write points");
        }
        if(comment == null || "".equals(comment) || comment.matches("[ ]*")){
            throw new Exception("Please write comment");
        }

        Points points1 = new Points()
                .setPoints(Long.parseLong(points))
                .setComment(comment)
                .setStudent(student)
                .setSubject(subject)
                .setTeacher(teacher);

        teacher.getPoints().add(points1);

        pointsRepository.save(points1);
        teacherRepository.save(teacher);
    }

    public void saveLetter(Teacher teacher, String theme, String letter, Student student) {
        Letter letter1 = new Letter()
                .setLetter(letter)
                .setTheme(theme)
                .setStudent(student)
                .setTeacher(teacher)
                .setFromStudent(false);

        letterRepository.save(letter1);
    }
}
