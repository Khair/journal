package ru.itis.services.admin;
/*
* @author Rustem Khairutdinov
* @group 11-602
*/

import org.springframework.expression.ExpressionException;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.itis.dto.*;
import ru.itis.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.repositories.*;
import ru.itis.security.roles.Role;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class AdminAddService {

    @Autowired
    private InstitutionRepository institutionRepository;

    @Autowired
    private SubjectRepository subjectRepository;

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private TeacherRepository teacherRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public void saveInstitution(String name) throws Exception{
        if(name == null || "".equals(name) || name.matches("[ ]*")){
            throw new Exception("Please write name");
        }
        if(institutionRepository.findFirstByName(name).isPresent()){
            throw new Exception("This Institution already exist");
        }
        Institution institution = new Institution();
        institution.setName(name);
        institutionRepository.save(institution);
    }

    public void saveSubject(String name, CourseCheckDtoWrapper courseCheckDtoWrapper, Institution institution) throws Exception {
        if(name == null || "".equals(name) || name.matches("[ ]*")){
            throw new Exception("Please write name");
        }
        if(subjectRepository.findFirstByNameAndInstitution_Id(name,institution.getId()).isPresent()){
            throw new Exception("This Subject already exist");
        }
        Subject subject = new Subject();
        subject.setName(name);
        List<CourseCheckDto> courseCheckDtoList = courseCheckDtoWrapper.getCourseCheckDtoList();
        for (int i = 0; i <  courseCheckDtoList.size(); i++) {
            if (courseCheckDtoList.get(i).getChecked() != null && courseCheckDtoList.get(i).getChecked()){
                Optional<Course> course = courseRepository.findById(Long.parseLong(courseCheckDtoList.get(i).getCourseid()));
                subject.getCourses().add(course.orElseThrow(() -> new ExpressionException("Don't touch html, thanks")));
            }
        }
        if(subject.getCourses().isEmpty()) throw new Exception("Add courses please");
        subject.setInstitution(institution);
        subjectRepository.save(subject);
    }

    public void saveGroup(String name, Course course, Institution institution) throws Exception {
        if(name == null || "".equals(name) || name.matches("[ ]*")){
            throw new Exception("Please write name");
        }
        if(subjectRepository.findFirstByName(name).isPresent()){
            throw new Exception("This Subject already exist");
        }

        Group group = new Group()
                .setName(name)
                .setCourse(course)
                .setInstitution(institution);
        groupRepository.save(group);
    }

    public List<Institution> getInstitutions() throws Exception {
        List<Institution> institutions = institutionRepository.findAll();
        if(institutions == null || institutions.isEmpty()){
            throw new Exception("does not exists, you should create them");
        }
        return institutions;
    }

    public List<Course> getCourses() throws Exception {
        List<Course> courses = courseRepository.findAll();
        if(courses == null || courses.isEmpty()){
            throw new Exception("does not exists, you should create them");
        }
        return courses;
    }

    public Institution getInstitutionById(String institutionId) throws Exception {
        Optional<Institution> institution = institutionRepository.findById(Long.parseLong(institutionId));
        if(!institution.isPresent()){
            throw new Exception("does not exists, you should create them or choose correct");
        }
        return institution.get();
    }

    public CourseCheckDtoWrapper getCourseCheckDtoWrapper(List<Course> courses) {
        List<CourseCheckDto> courseCheckDtoList = courses.stream()
                .map(course -> {
                    CourseCheckDto courseCheckDto = new CourseCheckDto();
                    courseCheckDto.setCourseid(course.getId().toString());
                    courseCheckDto.setName(course.getName());
                    courseCheckDto.setChecked(true);
                    return courseCheckDto;
                })
                .collect(Collectors.toList());
        return new CourseCheckDtoWrapper(courseCheckDtoList);
    }

    public Course getCourseById(String courseId) throws Exception {
        Optional<Course> course = courseRepository.findById(Long.parseLong(courseId));
        if(!course.isPresent()){
            throw new Exception("does not exists, you should create them or choose correct");
        }
        return course.get();
    }

    public List<Group> getGroups() throws Exception {
        List<Group> groups = groupRepository.findAll();
        if(groups == null || groups.isEmpty()){
            throw new Exception("does not exists, you should create them");
        }
        return groups;
    }

    public Group getGroupById(String groupId) throws Exception {
        Optional<Group> group = groupRepository.findById(Long.parseLong(groupId));
        if(!group.isPresent()){
            throw new Exception("does not exists, you should create them or choose correct");
        }
        return group.get();
    }

    public void saveStudent(StudentDto studentDto, Group group) {
        Student student = new Student();
        student
                .setFirstName(studentDto.getFirstName())
                .setSecondName(studentDto.getSecondName())
                .setFatherName(studentDto.getFatherName())
                .setBirthday(studentDto.getBirthday())
                .setCitizenship(studentDto.getCitizenship())
                .setBirthPlace(studentDto.getBirthCity())
                .setGroup(group);

        User user = new User();
        user
                .setLogin(student.getFirstName() + student.getBirthday())
                .setPassword(passwordEncoder.encode(student.getFirstName() + student.getBirthday() + 1001))
                .setRole(Role.USER);

        student.setUser(user);

        studentRepository.save(student);
    }

    public GroupAndSubjectWrapper getGroupAndSubjectWrapper(List<Group> groups, List<Subject> subjects) {
        List<GroupCheckDto> groupCheckDtoList = groups.stream()
                .map(group -> {
                    GroupCheckDto groupCheckDto = new GroupCheckDto();
                    groupCheckDto.setGroupid(group.getId().toString());
                    groupCheckDto.setName(group.getName());
                    groupCheckDto.setChecked(true);
                    return groupCheckDto;
                })
                .collect(Collectors.toList());
        List<SubjectCheckDto> subjectCheckDtoList = subjects.stream()
                .map(subject -> {
                    SubjectCheckDto subjectCheckDto = new SubjectCheckDto();
                    subjectCheckDto.setSubjectid(subject.getId().toString());
                    subjectCheckDto.setName(subject.getName());
                    subjectCheckDto.setChecked(true);
                    return subjectCheckDto;
                })
                .collect(Collectors.toList());
        return new GroupAndSubjectWrapper(groupCheckDtoList, subjectCheckDtoList);
    }

    public void saveTeacher(GroupAndSubjectWrapper groupAndSubjectWrapper) throws Exception {
        if(groupAndSubjectWrapper.getFirstname() == null || "".equals(groupAndSubjectWrapper.getFirstname()) || groupAndSubjectWrapper.getFirstname().matches("[ ]*")){
            throw new Exception("Please write first name");
        }
        if(groupAndSubjectWrapper.getSecondname() == null || "".equals(groupAndSubjectWrapper.getSecondname()) || groupAndSubjectWrapper.getSecondname().matches("[ ]*")){
            throw new Exception("Please write second name");
        }
        if(groupAndSubjectWrapper.getBirthday() == null || "".equals(groupAndSubjectWrapper.getBirthday()) || groupAndSubjectWrapper.getBirthday().matches("[ ]*")){
            throw new Exception("Please write birthday");
        }
        if(groupAndSubjectWrapper.getBirthplace() == null || "".equals(groupAndSubjectWrapper.getBirthplace()) || groupAndSubjectWrapper.getBirthplace().matches("[ ]*")){
            throw new Exception("Please write birth city");
        }
        if(groupAndSubjectWrapper.getCitizenship() == null || "".equals(groupAndSubjectWrapper.getCitizenship()) || groupAndSubjectWrapper.getCitizenship().matches("[ ]*")){
            throw new Exception("Please write citizenship");
        }
        Teacher teacher = new Teacher();
        teacher.setFirstName(groupAndSubjectWrapper.getFirstname())
                .setSecondName(groupAndSubjectWrapper.getSecondname())
                .setFatherName(groupAndSubjectWrapper.getFathername())
                .setBirthday(groupAndSubjectWrapper.getBirthday())
                .setBirthPlace(groupAndSubjectWrapper.getBirthplace())
                .setCitizenship(groupAndSubjectWrapper.getCitizenship());

        List<Group> groups = new ArrayList<>();

        List<GroupCheckDto> groupCheckDtoList = groupAndSubjectWrapper.getGroupCheckDtos();
        for (int i = 0; i <  groupCheckDtoList.size(); i++) {
            if (groupCheckDtoList.get(i).getChecked() != null && groupCheckDtoList.get(i).getChecked()){
                Optional<Group> group = groupRepository.findById(Long.parseLong(groupCheckDtoList.get(i).getGroupid()));
                teacher.getGroups().add(group.orElseThrow(() -> new ExpressionException("Don't touch html, thanks")));
                Group group1 = group.get();
                group1.getTeachers().add(teacher);
                groups.add(group1);
            }
        }

        List<Subject> subjects = new ArrayList<>();

        List<SubjectCheckDto> subjectCheckDtos = groupAndSubjectWrapper.getSubjectCheckDtos();
        for (int i = 0; i <  subjectCheckDtos.size(); i++) {
            if (subjectCheckDtos.get(i).getChecked() != null && subjectCheckDtos.get(i).getChecked()){
                Optional<Subject> subject = subjectRepository.findById(Long.parseLong(subjectCheckDtos.get(i).getSubjectid()));
                teacher.getSubjects().add(subject.orElseThrow(() -> new ExpressionException("Don't touch html, thanks")));
                Subject subject1 = subject.get();
                subject1.getTeachers().add(teacher);
                subjects.add(subject1);
            }
        }

        if(teacher.getSubjects().isEmpty()) throw new Exception("Add subjects please");
        if(teacher.getGroups().isEmpty()) throw new Exception("Add groups please");



        User user = new User();
        user
                .setLogin(teacher.getFirstName() + teacher.getBirthday())
                .setPassword(passwordEncoder.encode(teacher.getFirstName() + teacher.getBirthday() + 1001))
                .setRole(Role.TEACHER);

        teacher.setUser(user);

        teacherRepository.save(teacher);
        for (int i = 0; i < groups.size(); i++) {
            groupRepository.save(groups.get(i));
        }
        for (int i = 0; i < subjects.size(); i++) {
            subjectRepository.save(subjects.get(i));
        }
    }
}
