package ru.itis.services;
/*
* @author Rustem Khairutdinov
* @group 11-602
*/

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import ru.itis.models.User;
import ru.itis.security.details.UserDetailsImpl;

@Service
public class AuthenticationService {

    public User getUserByAuthentication(Authentication auth) {
        UserDetailsImpl currentUserDetails = (UserDetailsImpl) auth.getPrincipal();
        return currentUserDetails.getUser();
    }
}
