package ru.itis.services.student;
/*
* @author Rustem Khairutdinov
* @group 11-602
*/

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.models.*;
import ru.itis.repositories.LetterRepository;
import ru.itis.repositories.StudentRepository;
import ru.itis.repositories.TeacherRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class StudentService {

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private TeacherRepository teacherRepository;

    @Autowired
    private LetterRepository letterRepository;

    public Student getStudentByUserId(Long id) {
        return studentRepository.findByUser_Id(id).get();
    }

    public List<Subject> getSubjects(Student student){

        List<Subject> instSubs = student.getGroup().getInstitution().getSubjects();
        List<Subject> courseSubs = student.getGroup().getCourse().getSubjects();
        List<Subject> teacherSubs = student.getGroup().getTeachers().stream()
                .map(Teacher::getSubjects)
                .flatMap(Collection::stream)
                .distinct()
                .collect(Collectors.toList());

        List<Subject> temp = intersactionSubjects(instSubs, courseSubs);
        List<Subject> result = intersactionSubjects(temp, teacherSubs);

        return result;
    }

    private List<Subject> intersactionSubjects(List<Subject> subjects, List<Subject> subjects2){
        List<Subject> tempList = new ArrayList<>();

        for (int i = 0; i < subjects.size(); i++) {
            int count = 0;
            for (int j = 0; j < subjects2.size(); j++) {
                if(subjects.get(i).getId().equals(subjects2.get(j).getId()))
                    count = 1;
            }
            if(count > 0)
                tempList.add(subjects.get(i));
        }
        return tempList;
    }

    public Teacher getTeacherById(String teacherId) throws Exception {
        Optional<Teacher> teacher = teacherRepository.findById(Long.parseLong(teacherId));
        if(!teacher.isPresent()){
            throw new Exception("does not exists, you should create them or choose correct");
        }
        return teacher.get();
    }

    public void saveLetter(Teacher teacher, String theme, String letter, Student student) {
        Letter letter1 = new Letter()
                .setLetter(letter)
                .setTheme(theme)
                .setStudent(student)
                .setTeacher(teacher)
                .setFromStudent(true);

        letterRepository.save(letter1);
    }
}
