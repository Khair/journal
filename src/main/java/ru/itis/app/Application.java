package ru.itis.app;
/*
* @author Rustem Khairutdinov 
* @group 11-602
*/

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(scanBasePackages = "ru.itis")
@EntityScan(basePackages = "ru.itis.models")
@EnableJpaRepositories(basePackages = "ru.itis.repositories")
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
