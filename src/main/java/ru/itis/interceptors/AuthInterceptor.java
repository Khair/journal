package ru.itis.interceptors;
/*
* @author Rustem Khairutdinov
* @group 11-602
*/

import ru.itis.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import ru.itis.repositories.UserRepository;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

@Component
public class AuthInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private UserRepository userRepository;

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable ModelAndView modelAndView) throws Exception {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if(authentication == null){
            super.postHandle(request, response, handler, modelAndView);
            return;
        }
        if (!(authentication instanceof AnonymousAuthenticationToken)){
            String name  = authentication.getName();
            Optional<User> user = userRepository.findOneByLogin(name);
            request.setAttribute("auth_user", user.get());
        }
        super.postHandle(request, response, handler, modelAndView);
    }
}
